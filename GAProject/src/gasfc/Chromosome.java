package gasfc;

import java.util.ArrayList;
import java.util.Random;

/*
 * Trieda reprezentuje Chromozom v GA, respektive vec s cenou a vahou
 *
 * Autor: Tomas Kucera, xkucer90
 * Projekt do predmetu SFC 2017
 */
public class Chromosome {
	
	private int[] included;
	private int fitness;
	
	private int capacity;
	
	Chromosome(int[] included, int[] profit, int[] weight, int capacity) {
		this.included = included;
		this.fitness = 0;
		this.capacity = capacity;
		
		calculateFitness(profit, weight);
	}
	
	/*
	 * vypocitanie hodnoty fitness funkcie
	 */
	private void calculateFitness(int[] profit, int[] weight) {
		
		/*
		// Fitness funkcia pre minimalizaciu casu a ceny
		int actualWeight = 0, actualProfit = 0, totalProfit = 0;
		for (int i = 0; i < weight.length; i++) {
			actualWeight += included[i]*weight[i];
			actualProfit += included[i]*profit[i];
			totalProfit += profit[i];
		}

		fitness = 2*(1.0 / (1 + Math.abs(actualWeight - capacity))) + (1.0 / (actualProfit));
		*/
		
		/*
		 * Fitness funkcia so zapornymi hodnotami
		 int totalWeight = 0, actualWeight = 0;
		for (int i = 0; i < weight.length; i++) {
			fitness += included[i]*profit[i];
			totalWeight += weight[i];
			actualWeight += weight[i]*included[i];
		}
		
		fitness = fitness - Math.abs(actualWeight-Size);
		*/
		
		/*
	 	int actualWeight = 0;
		for (int i = 0; i < weight.length; i++) {
			fitness += included[i]*profit[i];
			actualWeight += included[i]*weight[i];
		}
		fitness = actualWeight > capacity ? 0 : fitness;	
		
		 */
		
		Random random = new Random();
		ArrayList<Integer> onePos = new ArrayList<Integer>();
		int actualWeight;
		int rnd;
		
		// pokym sa fitness hodnota nezdvihne nad 0
		while (true) {
			actualWeight = 0;
			onePos.clear();
			fitness = 0;
			
			// vypocet aktualnej vahy a zistenie kde su jednotky v included pre dalsie spracovanie
			for (int i = 0; i < weight.length; i++) {
				fitness += included[i]*profit[i];
				actualWeight += included[i]*weight[i];
				if (included[i] == 1) {
					onePos.add(i);
				}
			}
			
			fitness = actualWeight > capacity ? 0 : fitness;
			
			if (fitness != 0 || onePos.size() == 0) {
				break;
			}
			
			// uprava included - snaha o zvysenie fitness a opakovanie cyklu
		
			
			rnd = random.nextInt(onePos.size());
			included[onePos.get(rnd)] = 0;

		}
		
		
	}
	
	/*
	 * getter na included vektor -> indikuje ktore veci su v batohu
	 */
	public int[] getIncluded() {
		return included;
	}
	
	/*
	 * getter na hodnotu fitness funkcie
	 */
	public int getFitness() { 
		return fitness;
	}
	
	
}
