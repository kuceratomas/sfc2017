package gasfc;

import java.util.Arrays;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.control.TextArea;


/*
 * Trieda pre tlacenie vysledkov uzivatelovi
 *
 * Autor: Tomas Kucera, xkucer90
 * Projekt do predmetu SFC 2017
 */
public class Printer {
	
	private TextArea txtArea;
	private TextArea txtResult;
	
	public Printer(TextArea ta, TextArea tr) {
		txtArea = ta;
		txtResult = tr;
	}
	
	/*
	 * tlac do output logu - populacia a clenovia
	 */
	public void printPop(List<Chromosome> pop, long genIndex) {
		StringBuilder strBuilder = new StringBuilder("Generation " + genIndex + ":\n");
		int n = 0;
		for (Chromosome chrom : pop) {
			strBuilder.append(n).append(". ").append(Arrays.toString(chrom.getIncluded()) + " fit = " + chrom.getFitness() + "\n");
			n++;
		}
		strBuilder.append("\n");
		Platform.runLater(() -> txtArea.appendText(strBuilder.toString()));
	}
	
	/*
	 * tlac do Result logu
	 */
	public void printResults(List<Chromosome> pop, int termCond, long maxFittest, int graphDataCount,
			long genIndex, long bestInGeneration, Chromosome best) {
		String term;
		
		if (termCond == 0) {
			term = "Max gen. reached";
		}
		else if (termCond == 1) {
			term = "Fittest unchaged for " + maxFittest + " gen.";
		}
		else {
			term = "Manually terminated";
		}
			
		
		StringBuilder strBuilder = new StringBuilder("==============================\n");
		strBuilder.append("Results of the run: " + graphDataCount + "\n");
		strBuilder.append("Termination reason: " + term + "\n");
		strBuilder.append("Total amount of gen.: " + (genIndex+1) + "\n");
		strBuilder.append("Fittest found in gen.: " + bestInGeneration + "\n");
		strBuilder.append("Fittest value: " + best.getFitness() + "\n");
		strBuilder.append("Fittest vector: " + Arrays.toString(best.getIncluded()) + "\n");
		
		/*
		strBuilder.append("Items included in the fittest:\n");
		
		int[] bestIncluded = best.getIncluded();
		for (int i=0; i<bestIncluded.length; i++) {
			if (bestIncluded[i] == 1 ) {
				strBuilder.append(i+ ". profit: " + profit[i] + ", weight: " + weight[i] + "\n");
			}
		}
		*/
		
		
		Platform.runLater(() -> txtResult.appendText(strBuilder.toString()));
	}
}
