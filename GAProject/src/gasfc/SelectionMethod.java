package gasfc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/*
 * Trieda reprezentujuca vyber rodicov do tzv. mating poolu na krizenie
 *
 * Autor: Tomas Kucera, xkucer90
 * Projekt do predmetu SFC 2017
 */
public class SelectionMethod {
	
	private int genSize;
	private int eliteSize;
	private Recombination recomb;
	
	public SelectionMethod(int gs, int es, Recombination r) {
		genSize = gs;
		eliteSize = es;
		recomb = r;
	}
	
	/*
	 * vyber na principe turnaja - vyberu sa dvaja, ten s vacsou fitness ide do mating pool
	 */
	public List<Chromosome> createTournament(List<Chromosome> pop) {
		
		List<Chromosome> contenders = new ArrayList<Chromosome>();
		List<Chromosome> tempPop = new ArrayList<Chromosome>(pop);
		pop.subList(eliteSize, pop.size()).clear();
		
		Random random = new Random();
		
		// vygenerovanie dostatocne velkej populacie k elitam
		while (pop.size() < genSize) {
			// vybratie 2 sutaziacich
			while (contenders.size() != 2) {
				int rnd = random.nextInt(genSize);
				Chromosome contender1 = tempPop.get(rnd);
				rnd = random.nextInt(genSize);
				Chromosome contender2 = tempPop.get(rnd);
				
				// aby nebol 2x rovnaky -> vznikol by rovnaky potomok
				while (contender1 == contender2) {
					rnd = random.nextInt(genSize);
					contender2 = tempPop.get(rnd);
				}
				if (contender1.getFitness() > contender2.getFitness()) {
					contenders.add(contender1);
				}
				else {
					contenders.add(contender2);
				}
				if ((contenders.size() == 2) && (contenders.get(0) == contenders.get(1))) {
					contenders.remove(1);
				}
			}
			pop.add(recomb.crossover(contenders.get(0), contenders.get(1)));
			pop.add(recomb.crossover(contenders.get(1), contenders.get(0)));
			
			contenders.clear();
		}
		
		// neparna velkost populacie, jeden von
		if (pop.size() > genSize) {
			pop.remove(pop.size()-1);
		}
		
		return pop;
		
	}
	
	/*
	 * vyber na principe rulety -> nahodne podla velkosti fitness
	 */
	public List<Chromosome> createRoulette(List<Chromosome> pop) {
		
		List<Chromosome> tempPop = new ArrayList<Chromosome>(pop);
		pop.subList(eliteSize, pop.size()).clear();
		Chromosome parent1;
		Chromosome parent2;
		
		// total fitness
		double fsum = 0;
		for (int i = 0; i < genSize; i++) {
			fsum += tempPop.get(i).getFitness();
		}
		
		// dostatocne velka populacie k elitam
		while (pop.size() < genSize) {
			
			if (fsum == 0.0) {
				parent1 = tempPop.get(0);
				parent2 = tempPop.get(0);
			}
			else {
				parent1 = selectRoulette(fsum, tempPop);
				parent2 = selectRoulette(fsum, tempPop);
				int i = 0;
				while ((parent2.getIncluded().equals(parent1.getIncluded())) && (i < genSize)) {
					parent2 = selectRoulette(fsum, tempPop);
					i++;
				}
			}
			
			pop.add(recomb.crossover(parent1, parent2));
			pop.add(recomb.crossover(parent2, parent1));
	
			
		}
		
		// neparny pocet -> jeden von
		if (pop.size() > genSize) {
			pop.remove(pop.size()-1);
		}
		
		return pop;
		
	}
	
	// pomocna funkcia pre ruletovy vyber -> vyber clena
	private Chromosome selectRoulette(double fsum, List<Chromosome> tempPop) {

		double rnd = Math.random();
		
		double sum = 0;
		for (int i = 0; i < genSize; i++) {
			sum += (tempPop.get(i).getFitness() / fsum);
			if (sum >= rnd) {
				return tempPop.get(i);
			}			
		
		}
		
		return null;	
	}
	
	/*
	 * vyber na principe elity
	 */
	public List<Chromosome> createElite(List<Chromosome> pop) {
		
		// vyber rodicov podla fitness
		List<Chromosome> parents = new ArrayList<Chromosome>(pop);
		parents.subList(genSize-eliteSize, pop.size()).clear();
		
		// elitizmus
		pop.subList(eliteSize, pop.size()).clear();

		// rekombinacia
		int i = 0;
		while (pop.size() != genSize) {
			Chromosome parent1 = parents.get(i);
			Chromosome parent2 = parents.get(parents.size()-i-1);
			
			// neparny pocet -> na konci vybralo rovnakeho jedinca
			if (parents.size()-i-1 == i) {
				parent1 = parents.get(0);
				pop.add(recomb.crossover(parent1, parent2));
			}
			else {
				pop.add(recomb.crossover(parent1, parent2));
				pop.add(recomb.crossover(parent2, parent1));
			}

			i++;
			
		}
		
		return pop;
		
	}
}
