package gasfc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
 * Autor: Tomas Kucera, xkucer90
 * Projekt do predmetu SFC 2017
 */
public class Main extends Application{

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage mainStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("Scene.fxml"));
		mainStage.setTitle("Genetic Algorithm - The Knapsack Problem");
        Scene appScene = new Scene(root, 1024, 768);
        mainStage.setScene(appScene);
        mainStage.minWidthProperty().set(1024);
        mainStage.minHeightProperty().set(768);
        mainStage.show();
		
	}

}
