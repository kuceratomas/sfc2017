package gasfc;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/*
 * Trieda reprezentuje komunikacnu jednutku medzi javafx gui a backendom
 *
 * Autor: Tomas Kucera, xkucer90
 * Projekt do predmetu SFC 2017
 */
public class Controller implements Initializable {
	
	@FXML
    private TextArea txtArea, txtResult;
	
	@FXML
	private LineChart<Long, Double> chart;
	
	@FXML
	private Label lblTime;
	
	@FXML
    private Button btnStart, btnGen, btnReset;
	
	@FXML
	StackPane layout;
	
	@FXML
	RadioButton rdElite, rdRoul, rdTour;
	
	@FXML
	TextField txtProfit, txtWeight, txtElite, txtItems,txtMinW, txtMaxW, txtMinP, txtMaxP,
		txtW, txtP, txtCapacity, txtMutation, txtGenSize, txtMaxGen, txtFittest;
	
	
	private int graphDataCount = 0;
	
	private Evolution evolution;
	
	private Timeline clock ;
	
	private long stime;
    
	/*
	 * (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
    public void initialize(URL arg0, ResourceBundle arg1) {
    	
    	// nastavenie pociatocnych hodnot textovych poli pre rychle spustenie
    	//chart.setCreateSymbols(false);
    	txtP.setText("6, 5, 8, 9, 6, 7, 3");
    	txtW.setText("2, 3, 6, 7, 5, 9, 4");
    	txtElite.setText("2");
    	txtMutation.setText("0.001");
    	txtGenSize.setText("10");
    	txtMaxGen.setText("100");
    	txtFittest.setText("100");
    	txtItems.setText("7");
    	txtMinW.setText("1");
    	txtMaxW.setText("500");
    	txtMinP.setText("1");
    	txtMaxP.setText("500");
    	txtCapacity.setText("100");
    	
    	// needitovatelne a nefokusovatelne textove polia
    	txtArea.setEditable(false);
    	txtResult.setEditable(false);
    	txtArea.setFocusTraversable(false);
    	txtResult.setFocusTraversable(false);
    	
    	// nastavenie casovaca
    	DateFormat timeFormat = new SimpleDateFormat( "mm : ss . SSS" );
    	clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {            
            lblTime.setText( timeFormat.format( System.currentTimeMillis() - stime ) );

        }),
             new KeyFrame(Duration.millis(1))
        );
    	clock.setCycleCount( Animation.INDEFINITE );
    	
    	/// generovanie random profit a weight vektorov
    	btnGen.setOnAction(e -> {
    		genRandomVectors();
    	});
    	
    	// resetovanie vysledkov, grafu a timeru
    	btnReset.setOnAction(e -> {
    		chart.getData().clear();
    		txtResult.clear();
    		txtArea.clear();
    		graphDataCount = 0;
    		lblTime.setText("00 : 00 . 000");
    	});
    	
    	
    	btnStart.setOnAction(e -> {
            if (btnStart.getText().equals("Stop")) {
            	// zastavenie vypoctu
                evolution.cancel();
            }
            else {
            	// spustenie vypoctu, ziskanie aktualneho casu a sputenie timeru
                if (calculate()) {
	                stime = System.currentTimeMillis();
	                clock.play();
                }
            }
        });
    	
    }
 
    /*
     * generovanie random Profit a Weight vektoru
     */
    private void genRandomVectors() {
    	int count = 7, minp = 2, maxp = 9, minw = 2, maxw = 9;
    	
    	try {
	    	count = Integer.parseInt(txtItems.getText());
	    	minp = Integer.parseInt(txtMinP.getText());
	    	minw = Integer.parseInt(txtMinW.getText());
	    	maxp = Integer.parseInt(txtMaxP.getText());
	    	maxw = Integer.parseInt(txtMaxW.getText());
	    	
	    	if (count < 2 || minp < 1 || minw < 1 || maxp <= minp || maxw <= minw) {
	    		throw(new Exception());
	    	}
	    	
    	} catch(Exception e) {
    		showAlert("Positive integer values expected, Max higher than Min, item count higher than 1.");
    		return;
    	}
    	
    	txtP.clear();
    	txtW.clear();
    	Random random = new Random();
    	for (int i = 0; i < count; i++) {
    		txtP.appendText((random.nextInt(maxp - minp) + minp) + ", ");
    	}
    	
    	for (int i = 0; i < count; i++) {
    		txtW.appendText((random.nextInt(maxw - minw) + minw) + ", ");
    	}
    }
    
    /*
     * metoda vyvoala stlacenim Start tlacidla - zaciatok vypoctu
     */
	private boolean calculate() {
    	
    	int[] p = null;
		int[] w = null;
    	int c, el, gs;
    	long mg, f;
    	double m;
    	int sel = 0;
    	
    	
    	// parsovanie vstupu od uzivatela
		try {
			c = Integer.parseInt(txtCapacity.getText());
	    	el = Integer.parseInt(txtElite.getText());
	    	m = Double.parseDouble(txtMutation.getText());
	    	gs = Integer.parseInt(txtGenSize.getText());
	    	mg = Integer.parseInt(txtMaxGen.getText());
	    	f = Integer.parseInt(txtFittest.getText());
	    	
	    	if (c < 1 || el < 0 || el > gs || m < 0.0 || m > 1.0 || gs < 1 || mg < 1  || f < 0) {
	    		throw(new Exception());
	    	}
    	} catch(Exception e) {
    		showAlert("Positive integer (double for mutation) values expected, capacity and max generations higher than 1, not more elites than gen. size, mutation chance between 0 and 1.");
    		return false;
    	}
    	
    	try {
    		p = Arrays.stream(txtP.getText().split("\\W+")).mapToInt(Integer::parseInt).toArray();
    		w = Arrays.stream(txtW.getText().split("\\W+")).mapToInt(Integer::parseInt).toArray();
    	} catch(Exception e) {
    		showAlert("Integer values delimited by \",\" or space \" \" expected.");
    		return false;
    	}
    	if (p.length != w.length) {
    		showAlert("Input Profit and Weight vectors must have the same length.");
    		return false;
    	}
    	
    	if (rdElite.isSelected())
    		sel = 0;
    	else if (rdRoul.isSelected())
    		sel = 1;
    	else
    		sel = 2;

    	txtArea.clear();
    	
    	// spustenie samotneho vypoctu v novom vlakne pre responzivne GUI
    	evolution = new Evolution(txtArea, txtResult, sel, p, w, c, el, m, gs, mg, f, chart, graphDataCount);
    	
    	evolution.setOnSucceeded(t -> {
    		btnStart.setText("Start");
    		clock.stop();
    	
    	});
    	evolution.setOnCancelled(t -> {
    		btnStart.setText("Start");
    		clock.stop();
    	});
    	
    	graphDataCount++;
    	btnStart.setText("Stop");
    	new Thread(evolution).start();
    	
    	return true;
    
    }
    
    
    public static void showAlert(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Wrong Input.");
        alert.setHeaderText("Error!");
        alert.setContentText(msg);
        alert.showAndWait();
    }
    
    
}
