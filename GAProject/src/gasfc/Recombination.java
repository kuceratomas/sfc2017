package gasfc;

import java.util.Random;


/*
 * Trieda reprezentuje rekombinaciu, teda krizenie jedincov
 *
 * Autor: Tomas Kucera, xkucer90
 * Projekt do predmetu SFC 2017
 */
public class Recombination {
	
	private double mutation;
	private int[] profit;
	private int[] weight;
	private int capacity;
	
	public Recombination(double m, int[] p, int[] w, int c) {
		mutation = m;
		profit = p;
		weight = w;
		capacity = c;
	}
	
	/* 
	 * samotny proces krizenia
	 */
	public Chromosome crossover(Chromosome chrom1, Chromosome chrom2) {
		
		int[] included1 = chrom1.getIncluded();
		int[] included2 = chrom2.getIncluded();
		int[] childIncluded = new int[included1.length];
		
		// random bod krizenia
		Random random = new Random();
		int crossoverPoint = random.nextInt(included1.length - 1) + 1;

		// krizenie
		for (int i = 0; i < included1.length; i++) {
            if (i < crossoverPoint) {
            	childIncluded[i] = included1[i];
            } else {
            	childIncluded[i] = included2[i];
            }
        }
		
		// mutacia podla zvolenej pravdepodobnosti
		if (Math.random() < mutation) {
			childIncluded = mutate(childIncluded);
		}
			
		return new Chromosome(childIncluded, profit, weight, capacity);
	}
	
	/*
	 * samotny proces mutacie
	 */
	private int[] mutate(int[] included) {
		Random random = new Random();
		int rnd = random.nextInt(included.length - 1);
		included[rnd] = (included[rnd] == 1) ? 0 : 1;
		
		return included;
	}
	
}
