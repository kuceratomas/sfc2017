package gasfc;

import java.util.Comparator;


/*
 * Pomocna trieda pre porovnavanie fitness hodnot jedincov pre Collections.sort
 *
 * Autor: Tomas Kucera, xkucer90
 * Projekt do predmetu SFC 2017
 */
public class FitnessComparator implements Comparator<Chromosome> {

    @Override
    public int compare(Chromosome chrom1, Chromosome chrom2) {
    	
        return chrom2.getFitness() - chrom1.getFitness();
    }
}