package gasfc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextArea;


/*
 * Trieda reprezentuje Evoluciu - riadi proces vzniku jedincov, krizenia
 * mutacie a vypis pre uzivatela
 *
 * Autor: Tomas Kucera, xkucer90
 * Projekt do predmetu SFC 2017
 */
public class Evolution extends Task<Object> {
	
	private SelectionMethod selMethod;
	
	private Printer printer;
	
	private int[] profit;
	private int[] weight;
	private int capacity;
	private int graphDataCount;
	private int genSize;
	private long maxGen;
	private long maxFittest;
	private Chromosome best;
	private int bestLifeSpan;
	private int selection;
	
	private List<Chromosome> pop; 
	long genIndex;
	
	private LineChart<Long, Double> chart;
	
	/*
	 * konstruktor pre inicializaciu
	 */
	public Evolution(TextArea txtArea, TextArea txtResult, int sel, int[] p, int[] w, int c, int el, double m, int gs, long mg, long f, LineChart<Long, Double> chart, int graphDataCount) {

		this.chart = chart;
		this.graphDataCount = graphDataCount;
		this.profit = p;
		this.weight = w;
		this.capacity = c;
		this.genSize = gs;
		this.maxFittest = f;
		this.maxGen = mg;
		this.selection = sel;

		Recombination recomb = new Recombination(m, p, w, c);
		selMethod = new SelectionMethod(gs, el, recomb);
		printer = new Printer(txtArea, txtResult);
		pop = new ArrayList<Chromosome>();
		genIndex = 0;
 
	}

	@Override
	protected Object call() throws Exception {
		
		int termCond = 0;
		double meanFit = 0.0;
		long bestInGeneration = 0;
		
		XYChart.Series<Long, Double> series = new XYChart.Series<Long, Double>();
		series.setName(Integer.toString(graphDataCount));
		
		// vytvorenie pociatocnej populacie
		for (int i = 0; i < genSize; i++) {
			pop.add(new Chromosome(getRandomArray(), profit, weight, capacity));		
		}
		
		Collections.sort(pop, new FitnessComparator());
		best = pop.get(0); // best so far je prvy zo zoradenej init generacie
		bestLifeSpan = 1;
		
		printer.printPop(pop, genIndex);
		
		for (int i=0; i < genSize; i++) {
			meanFit += pop.get(i).getFitness();
		}
		meanFit /= genSize;
		
		series.getData().add(new XYChart.Data<Long, Double>(0L, meanFit));
		
		// cyklus pre vznik generacii
		for (int i = 1; i < maxGen; i++) {
			meanFit = 0;
			genIndex++;
				
			// rozhodovanie ktora metoda vyberu bude pouzita
			if (selection == 0) {
				pop = selMethod.createElite(pop);
			}
			else if (selection == 1) {
				pop = selMethod.createRoulette(pop);
			}
			else {
				pop = selMethod.createTournament(pop);
			}
				
			Collections.sort(pop, new FitnessComparator());
			
			if (maxGen < 50) {
				for (int j=0; j < genSize; j++) {
					meanFit += pop.get(j).getFitness();
				}
				meanFit /= genSize;
				
				series.getData().add(new XYChart.Data<Long, Double>(genIndex, meanFit));
			}
			
			// zistenie best so far najlepsieho individua a vypis vysledkov
			bestLifeSpan++;
			if (pop.get(0).getFitness() > best.getFitness()) {
				best = pop.get(0);
				bestInGeneration = genIndex;
				bestLifeSpan = 0;
				
				printer.printPop(pop, genIndex);
				
				if (maxGen >= 50) {
					for (int j=0; j < genSize; j++) {
						meanFit += pop.get(j).getFitness();
					}
					meanFit /= genSize;
					
					series.getData().add(new XYChart.Data<Long, Double>(genIndex, meanFit));
				}
		
			}
			
			// kontrola zivota najlepsieho individua
			if (bestLifeSpan > maxFittest) {
				termCond = 1;
				break;
			}
			
			// kontrola manualneho zrusenia vlakna uzivatelom
			if (this.isCancelled()) {
				termCond = 2;
				break;
			}
			
		}
		
		// vypis finalnych vysledkov a vykreslenie dat grafom
		printer.printResults(pop, termCond, maxFittest, graphDataCount, genIndex, bestInGeneration, best);
		Platform.runLater(() -> chart.getData().add(series));

		
		return null;
	}
	
	
	/*
	 * vytvorenie random included pola (indikacia ci je vec v batohu alebo nie)
	 */
	private int[] getRandomArray() {
		int[] array = new int[weight.length];
		
		while (true) {
			int zeroCount = 0;
			for (int i = 0; i < weight.length; i++) {
				array[i] = (int) Math.round(Math.random());
				if (array[i] == 0) zeroCount++;
			}
			// nepovolena ziadna vec v batohu
			if (zeroCount != array.length) {
				break;
			}
		}

		return array;
	}

	
}
